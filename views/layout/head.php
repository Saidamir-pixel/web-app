<?php 
include('../../controllers/authController.php') 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body>
<?php
if($_SESSION['id'] != NULL): 
?>

<!-- menu button -->
<div class="flex text-gray-800">
    <ul id='menu' class="form-signin">
        <li><a id='menu_btn' href="#">Menu</a>
            <ul>
                <li><a id='menu_btn' href='/views/posts/index.php'>My page</a></li>
                <li><a id='menu_btn' href='/views/main/index.php'>Main</a></li>
                <li><?php logout() ?><a href="/?logout=<?= 'Logout' ?>">Logout</a></li>
            </ul>
        </li>
    </ul>
</div>
<?php endif ?>