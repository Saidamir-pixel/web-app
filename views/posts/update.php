<?php
include('../../controllers/postController.php');
$data = edited();
updated()
?>
<!-- edit button -->
<?php require('../../views/layout/head.php') ?>
<body>
<a style="max-width: 100px; margin-left: 10px;" class="btn btn-lg btn-primary btn-block my-2" href="index.php">Back</a>
<?php foreach ($data as $item): ?>
<div class="main_block">
    <form action="" class="form-signin" method="POST">
        <input type="text" name="title" class="form-control my-2" value="<?= $item['title'] ?>" required>
        <textarea rows="7" name="description" class="form-control my-2" required><?= $item['description'] ?></textarea>
        <button class="btn btn-lg btn-primary btn-block my-2" type="submit">Save</button>
    </form>
</div>
<?php endforeach ?>
</body>
</html>