<?php
include('../../controllers/postController.php');
$data = edited();
updated()
?>
<!-- more button -->
<?php require('../../views/layout/head.php') ?>
<body>
<a style="max-width: 100px; margin-left: 10px;" class="btn btn-lg btn-primary btn-block my-2" href="index.php">Back</a>
<div class="main_block">
    <?php foreach ($data as $item): ?>
    <div class="post">
        <h2 class="title"><?= $item['title'] ?></h2>
        <h2 class="desc"><?= $item['description'] ?></h2>
        <p class="desc">Date: <?= $item['date'] ?></p>
        <p class="desc">Author: <?= $item['name'] ?></p>
        <p class="like">Likes: <?= $item['likes'] ?></p>
    </div>
    <?php endforeach ?>
</div>
</body>
</html>