<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body>

<!-- welcome page -->
   <div>
      <p style="margin: auto; color: white; max-width: 500px; margin-top: 80px; margin-bottom: 80px; font-size: 30px;">Welcome to TheFaceNote.com. Here you can save posts and put them in a public window, where people like you can see them and share their own.</p>
      <a style="max-width:200px; margin: auto;" class="btn btn-lg btn-primary btn-block my-2" href="/views/auth/login.php">Login</a>
      <a style="max-width:200px; margin: auto;" class="btn btn-lg btn-primary btn-block my-2" href="/views/auth/regis.php">Registration</a>
   </div>
</body>


